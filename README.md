# Absorption data related to aqueous solutions of Tartrazine, Sunset Yellow and Amaranth

•	Amatrix.txt: File containing absorbances. Each line is associated with a different sample and each column is related with a given wavelength.

•	Lvector.txt: File listing the wavelengths where the absorbances were read.

•	Cmatrix.txt: File with reference values for the concentrations of each dye in the samples. The concentrations (in mg/L) of Tartrazine, Sunset Yellow and Amaranth were placed in columns 1–3, respectively.

•	CalibTartrazine.txt: This file contains the standard curve of Tartrazine. The concentrations are placed in the first column and the absorbances at 429, 482 and 523 nm are given in columns 2–4, respectively.

•	CalibSYellow.txt: Sunset Yellow standard curve.

•	CalibAmaranth.txt: Amaranth standard curve.

•	PureSpct.txt: Absorption spectra of aqueous solutions of the pure dyes. Lines 1–3 contain the absorbances of Tartrazine, Sunset Yellow and Amaranth, respectively. The corresponding wavelengths are those listed in “Lvector.txt” file.
